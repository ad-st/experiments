#include "Poco/Net/SocketAddress.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/SocketStream.h"
#include "Poco/StreamCopier.h"
#include <iostream>

int main(int argc, char **argv)
{
    using namespace Poco;
    using namespace Poco::Net;
    using namespace std;

    SocketAddress sa("www.google.com", 80);
    StreamSocket socket(sa);
    SocketStream str(socket);

    str << "GET / HTTP/1.1\r\n"
        << "Host: www.google.com\r\n"
        << "\r\n";
    
    str.flush();

    StreamCopier::copyStream(str, cout);

    return 0;
}