#include <optional>
#include <iostream>

using namespace std;

void test_raw()
{
  cout << "testing raw" << endl;

  int* i = 0;
  try
  {
    *i = 10;
  }
  catch (...)
  {
    cout << "bad assignment" << endl;
  }
  cout << "good bye" << endl;
}

void test_optional()
{
  cout << "testing optional" << endl;

  std::optional<int*> i;
  try
  {
    *i.value() = 10;
  }
  catch (...)
  {
    cout << "bad assignment" << endl;
  }
  cout << "good bye" << endl;
}


int main()
{
  test_optional();
  test_raw();
}