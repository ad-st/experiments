#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <type_traits>

std::vector<int> v{1,3,4,2,7,4,5,2};

template<class T> struct dependent_false : std::false_type {};

template <typename RandomAcccessIt>
void quick_sort(RandomAcccessIt left, RandomAcccessIt right)
{
    using category = typename std::iterator_traits<RandomAcccessIt>::iterator_category;
    if constexpr (std::is_same_v<category, std::random_access_iterator_tag>)
    {
        if (left < right)
        {
            RandomAcccessIt pivot = right-1;
            auto new_pivot_place = std::partition(left, right, [&](auto value)->auto {return value < *pivot;});
            std::swap(*pivot, *new_pivot_place);
            quick_sort(left, new_pivot_place-1);
            quick_sort(new_pivot_place+1, right);
        }
    }
    else
    {
        static_assert(dependent_false<RandomAcccessIt>::value, "Use only with random access iterators");
    }
    
}

int main()
{
    quick_sort(v.begin(), v.end());
    return 0;
}