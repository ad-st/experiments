#include <any>
#include <iostream>

using namespace std;

void print_int(any ints)
{
    try 
    {
        cout << any_cast<int>(ints) << endl;
    }
    catch(...)
    {
        cout << "you can't do that" << endl;
    }
}

void print_int_old(void* ints)
{
    try 
    {
        cout << *reinterpret_cast<int*>(ints) << endl;
    }
    catch(bad_any_cast&)
    {
        cout << "you can't do that" << endl;
    }
}

void main()
{
    int a = 50;
    print_int(a);
    print_int_old(&a);

    double b = 50;
    print_int(b);
    print_int_old(&b);
}