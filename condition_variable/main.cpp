#include <iostream>
#include <condition_variable>
#include <thread>

using namespace std;

struct Shared
{
    condition_variable cv;
    mutex mtx;
    bool ready=false;
} s;


void sleeping(){
    auto lck = unique_lock(s.mtx);
    while(!s.ready)
        s.cv.wait(lck);
}

int main(int, char**) {
    auto th1 = thread(sleeping);
    this_thread::sleep_for(50ms);
    s.ready = true;
    s.cv.notify_one();
    th1.join();
}
