#include <iostream>
#include <functional>
#include <string>
#include <vector>

using namespace std;

template <typename It>
It remove(It beg, It end, typename iterator_traits<It>::value_type val)
{
    auto pout = beg;
    for (auto &pin = beg; pin != end && pout != end; ++pin)
    {
        if (!(*pin == val))
        {
            *pout = move(*pin);
            ++pout;
        }
    }
    return pout;
}

int main()
{
    std::vector<int> vec{1, 2, 5, 2, 1, 6, 3, 1, 2, 54, 6, 1};
    vec.erase(remove(vec.begin(), vec.end(), 1), vec.end());
    cout << "Done" << endl;
}