#include <iostream>
#include <assert.h>

template <typename T> class my_shared_ptr
{
    struct ctrl_block 
    {
        size_t uses;
        T* ptr;
    };
    ctrl_block* ctrl;

    void decr()
    {
        if (ctrl && --ctrl->uses == 0)
        {
            delete ctrl->ptr;
            delete ctrl;
            ctrl = nullptr;
        }
    }
    void incr()
    {
        ctrl->uses++;
    }

    public:
    my_shared_ptr()
        : ctrl(nullptr)
    {

    }
    my_shared_ptr(T* ptr)
        : ctrl{new ctrl_block{1, ptr}}
    {
    }
    my_shared_ptr(const my_shared_ptr& rhs)
        : ctrl(rhs.ctrl)
    {
        incr();
    }
    my_shared_ptr(my_shared_ptr&& rhs)
    {
        *this = std::move(rhs);
    }
    ~my_shared_ptr()
    {
        decr();
    }
    my_shared_ptr& operator=(const my_shared_ptr& rhs)
    {
        if (rhs.ctrl != ctrl)
        {
            decr();
            ctrl = rhs.ctrl;
            incr();
        }
        return *this;
    }
    my_shared_ptr& operator=(my_shared_ptr&& rhs)
    {
        ctrl = rhs.ctrl;
        rhs.ctrl = nullptr;
        return *this;
    }
    T operator*() const
    {
        return *ctrl->ptr;
    }
    bool is_empty() const
    {
        return ctrl == nullptr;
    }
};

int main()
{
    {
        int* ptr = new int;
        *ptr = 10;
        my_shared_ptr<int> a(ptr);
        assert(*a == *ptr);
        {
            my_shared_ptr<int> b(a);
            assert(*a == *b);
        }
        assert(*a == *ptr);

        my_shared_ptr<int> c;
        assert(c.is_empty() == true);
        {
            my_shared_ptr<int> b(std::move(a));
            assert(*b == *ptr);
            assert(a.is_empty() == true);
            assert(b.is_empty() == false);

            c = std::move(b);
            assert(*c == *ptr);
            assert(b.is_empty() == true);
            assert(c.is_empty() == false);
        }
        assert(c.is_empty() == false);

        my_shared_ptr<int> d;
        {
            d = c;
            assert(*d == *ptr);
            assert(*c == *ptr);
            assert(d.is_empty() == false);
            assert(c.is_empty() == false);
        }
    }
    return 0;
}