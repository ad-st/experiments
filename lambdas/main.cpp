#include <functional>
#include <iostream>

using namespace std;

std::function<void()> give_me_one() {
  std::function<void()> one, two, three;

  one = [&]() {
    cout << "one" << endl;
    two();
  };

  two = [&]() {
    cout << "two" << endl;
    three();
  };

  three = [&]() { cout << "three" << endl; };

  return one;
}

struct A {
  void one() {
    cout << "one" << endl;
    two();
  }

  void two() {
    cout << "two" << endl;
    three();
  }

  void three() { cout << "three" << endl; }

  static std::function<void()> give_me_one(A *that) {
    return std::bind(&A::one, that);
  }
};

int main() {
  // what's wrong with this code?
  //auto one = give_me_one();
  //one();

  {
    A a;
    auto one = A::give_me_one(&a);
    one();
  }
}