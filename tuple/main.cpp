#include <iostream>
#include <tuple>
#include <string>

using namespace std;

int main()
{
    tuple<string, string, string> t1{"a", "b", "c"};
    tuple<string, string> t2{"d", "e"};
    tuple<int, int, int> t3{1,2,3};

    auto tc = tuple_cat(t1, t2,t3);
    auto [a,b,c,d,e, f, g, h] = tc;

    cout << a << b << c << d << e << f << g << h << endl;

    cout << tuple_size_v<decltype(tc)> << endl;
    cout << sizeof(tc) << endl;
}