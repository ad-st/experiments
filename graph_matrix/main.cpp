#include <iostream>
#include <array>
#include <vector>
#include <set>
#include <iostream>

template <char S>
struct graph
{
    size_t vertices[S][S]{};

    void connect_bidirect(char key1, char key2, char weight)
    {
        auto p1 = key1 - 'a';
        auto p2 = key2 - 'a';
        
        if (p1 >= 0 && p1 < S && p2 >= 0 && p2 < S)
        {
            vertices[p1][p2] += weight;
            vertices[p2][p1] += weight;
        }
    }

    void print()
    {
        for (int i = 0; i < S; i++)
        {
            for (int j = 0; j < S; j++)
            {
                std::cout << vertices[i][j] << " ";
            }
            std::cout << std::endl;
        }
    }
};

int main()
{
    graph<5> g;
    g.connect_bidirect('a','b', 6);
    g.connect_bidirect('a','d', 1);
    g.connect_bidirect('b','d', 2);
    g.connect_bidirect('b','e', 2);
    g.connect_bidirect('e','d', 1);
    g.connect_bidirect('b','c', 5);
    g.connect_bidirect('e','c', 5);
    g.print();
    return 0;
}