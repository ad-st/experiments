#include <iostream>
#include <atomic>
#include <thread>
#include <math.h>
#include <chrono>

void main()
{
    using namespace std;
    atomic<long> shared = 1;
    atomic<bool> finished = false;
    int first_loops=0, second_loops=0;
    int first_empty=0, second_empty=0;

    auto first = thread([&](){
        while (!finished)
        {
            auto expected = shared.load();
            while (!shared.compare_exchange_weak(expected, lround(sqrt(expected)) ))
                first_empty++;
            first_loops++;
        }
    });
    

    auto second = thread([&](){
        while (!finished)
        {
            auto expected = shared.load();
            while (!shared.compare_exchange_weak(expected, expected+1))
                second_empty++;
            second_loops++;
        }
    });

   this_thread::sleep_for(chrono::seconds(5));
   finished = true;
   first.join();
   second.join();

   cout << "first   loops - total: " << first_loops  << " empty: " << first_empty << endl;
   cout << "second  loops - total: " << second_loops << " empty: " << second_empty << endl;
}