#include "Poco/JSON/Parser.h"
#include "Poco/JSON/Object.h"
#include "Poco/JSON/Stringifier.h"
#include "Poco/Path.h"
#include "Poco/File.h"
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <filesystem>
#include <iostream>
#include <vector>
#include <random>

namespace
{
using namespace std;
using namespace std::filesystem;
namespace json = Poco::JSON;

struct verb final
{
    enum class form
    {
        foreign,
        first,
        second,
        third
    };
    string foreign;
    string first;
    string second;
    string third;

    string print(form show_only) const
    {
        return (show_only != form::foreign ? "***" : foreign) + "     " + (show_only != form::first ? "***" : first) + " " + (show_only != form::second ? "***" : second) + " " + (show_only != form::third ? "***" : third);
    }

    operator string() const
    {
        return foreign + ", " + first + ", " + second + ", " + third;
    }

    template <typename T>
    bool operator==(const T &rhs) const
    {
        return rhs.size() == 4 && rhs[0] == foreign && rhs[1] == first && rhs[2] == second && rhs[3] == third;
    }

    verb(const json::Object::Ptr source)
    {
        foreign = source->getValue<string>("foreign");
        first = source->getValue<string>("first");
        second = source->getValue<string>("second");
        third = source->getValue<string>("third");
    }
};

auto get_directory()
{
    const bool IsLinux =
#ifdef _WIN32
        false
#else
        true
#endif
        ;
    auto home_directory(path(Poco::Path::expand(IsLinux ? "$HOME" : "%HOMEDRIVE%%HOMEPATH%")));
    auto verbs_directory( home_directory / ".verbs");

    Poco::File(verbs_directory).createDirectories();

    return verbs_directory;
}

auto open_json_file(const path &verbs)
{
    Poco::Dynamic::Var result;

    fstream file(verbs, fstream::in | fstream::out | fstream::app);
    try
    {
        result = json::Parser().parse(file);
    }
    catch (json::JSONException &)
    {
        result = json::Parser().parse("[]");
    }
    return result.extract<json::Array::Ptr>();
}

auto get_random_verb(const json::Array::Ptr verb_array)
{
    static random_device rd;
    static mt19937 g(rd());
    auto verb_no = uniform_int_distribution<int>(0, verb_array->size() - 1)(g);
    return verb(verb_array->getObject(verb_no));
}

auto get_random_form()
{
    static vector<verb::form> possible_forms{verb::form::first, verb::form::second, verb::form::third, verb::form::foreign};
    shuffle(possible_forms.begin(), possible_forms.end(), random_device());
    return *possible_forms.begin();
}

enum class answers
{
    exit_program,
    correct,
    incorrect
};

auto get_answer(const string &response, const verb &verb_in_question)
{
    vector<string> result;
    boost::split(result, response, boost::is_any_of(","));
    for_each(begin(result), end(result), bind(&boost::trim<string>, placeholders::_1, locale()));

    if (result.empty() || result[0].empty() || result[0] == "exit")
        return answers::exit_program;

    return verb_in_question == result ? answers::correct : answers::incorrect;
}
}; // namespace

int main(int argc, char **argv)
{
    auto verbs_directory = get_directory();
    auto verbs_file = verbs_directory / "verbs";
    auto verb_array = open_json_file(verbs_file);
    if (verb_array->size() != 0)
    {
        while (true)
        {
            auto verb_in_question = get_random_verb(verb_array);
            auto form_in_question = get_random_form();

            cout << "Please type all verb forms. Separate them with comma. To end type exit" << endl
                 << endl;
            cout << verb_in_question.print(form_in_question) << endl;

            string response;
            getline(cin, response);

            switch (get_answer(response, verb_in_question))
            {
            case answers::exit_program:
                cout << "Thank you, come again!" << endl;
                return 0;
            case answers::correct:
                cout << "correct" << endl;
                break;
            case answers::incorrect:
                cout << "incorrec, "
                     << "correct form: " << (string)verb_in_question << endl
                     << endl;
                break;
            }
        }
    }
    else
    {
        cout << "verb file is empty, do you want to create it? (Y/n)" << endl;
        string response;
        getline(cin, response);
        if (response == "Y" || response == "y" || response.empty())
        {
            ofstream f(verbs_file);
            f << "[{\"foreign\": \"\",\"first\": \"\",\"second\": \"\",\"third\": \"\"}]" << endl;
            cout << "file created in " << verbs_file << endl;
        }
    }
    return 1;
}