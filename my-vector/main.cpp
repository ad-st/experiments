#include <iostream>
#include <assert.h>

template <typename T> class my_vector
{
    private:
    T* ptr_begin;
    size_t arr_size;
    size_t capacity;

    void allocate(size_t capacity)
    {
        ptr_begin = (T*)::operator new(capacity*sizeof(T));
        this->capacity = capacity;
    }
    void copy(const my_vector& rhs)
    {
        for (arr_size=0;arr_size < rhs.arr_size; arr_size++)
        {
            new (ptr_begin + arr_size) T(rhs.ptr_begin[arr_size]);
        }
    }
    void dealocate()
    {
        for (size_t i=0;i < arr_size; i++)
        {
            ptr_begin[i].~T();
        }
        ::operator delete(ptr_begin);
    }
    public:
    my_vector(size_t capacity=0)
    : arr_size(0)
    {
        allocate(capacity);
    }
    my_vector(const my_vector& rhs)
    {
        allocate(rhs.capacity);
        copy(rhs);
    }
    my_vector(my_vector& rhs, size_t capacity)
    {
        allocate(capacity);
        copy(rhs);
    }
    my_vector(my_vector&& rhs)
    {
        *this = std::move(rhs);
    }
    ~my_vector()
    {
        dealocate();
    }
    my_vector& operator=(const my_vector& rhs)
    {
        my_vector temp{rhs};
        *this = std::move(temp);
        return *this;
    }
    my_vector& operator=(my_vector&& rhs)
    {
        ptr_begin = rhs.ptr_begin;
        arr_size = rhs.arr_size;
        capacity = rhs.capacity;
        rhs.ptr_begin = nullptr;
        rhs.arr_size = 0;
        rhs.capacity = 0;
        return *this;
    }

    T& operator[](size_t index)
    {
        return ptr_begin[index];
    }
    void push_back(const T& rhs)
    {
        if (arr_size+1 > capacity)
        {
            my_vector temp(*this, capacity*2);
            *this = std::move(temp);
        }
        new (ptr_begin + arr_size++) T(rhs);
    }
    template <typename ... Args> void emplace_back(Args ... args)
    {
    }
    void insert(const T&);
    template <typename ... Args> void insert(Args ... args);
    size_t size()
    {
        return arr_size;
    }
};

int main()
{
    my_vector<int> a(10);
    for (int x=0; x < 11; x++)
    {
        a.push_back(10);
    }
    my_vector<int> b(a);

    assert(a.size() == b.size());
    assert(a.size() == 11);
    assert(a[0] == b[0]);

    {
        my_vector<int> c(std::move(a));
    }
    {
        my_vector<int> d;
        d = b;
    }
    return 0;
}