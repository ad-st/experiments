#include <functional>
#include <iostream>
#include <vector>

using namespace std;

void foo(int &i) {
  cout << i << endl;
  i++;
}

int main() {
  int i = 10;
  auto r = std::bind(foo, i);
  i = 1;

  r(); // - 10 because internal variable representing first parameter inside r
       // object is 10
  r(); // - 11 because internal variable inside r was incremented

  std::vector<std::function<void(int &)>> calls;
  calls.emplace_back(foo);
  calls.emplace_back(std::bind(foo, std::ref(i)));
  calls.emplace_back(std::bind(foo, i));
  calls.emplace_back(foo);

  for (auto &call : calls) {
    call(i);
  }
  // - 1 because it's value comes from i variable inside main scope
  // - 2 because again it's value comes from i variable, i was bounded to
  // reference_wrapper object
  // - 1 because this value comes from inside of call
  // object. std::bind didn't bound to any external variable, just merely set
  // it's initial value
  // - 3 because again it's from inside i variable
}